# Observability-Infrastructure

Applications to deploy on EKS 1.29 and ArgoCD: 
```
1 - Prometheus - Deployed
2 - Loki - Deployed
3 - Grafana - Deployed
4 - Mimir - Symlink error 
5 - Tempo - Deployed
```
# Resources: 
1. All helm charts here: https://gitlab.com/eks_argo_tf/apps/charts
2. EKS and ArgoCD TF pipeline here: https://gitlab.com/eks_argo_tf/eks_argo_deploy
Progress: 
1. Deployed EKS cluster, ArgoCD, external-dns, nginx-ingress cluster: https://gitlab.com/eks_argo_tf/eks_argo_deploy
2. Deployed test app successfully
3. Created App of apps repo: https://gitlab.com/eks_argo_tf/apps/observability-apps

# Issues fixed so far: 
1. I had permission issues from GitLab CI. Some the execution works from local terminal. I checked IAM role but it looks correct. 
2. AppsOfApps repo https://gitlab.com/eks_argo_tf/apps/observability-apps tried to deploy applications but missing permissions. This should be RBAC issue. 
3. I had issues to remove applications from Argo because of finalizers but then removed finalizers configurations from resources which were stuck. 
4. Removed Argo and re-deploying again to fix Argo rbac issue.
5. When deploying Kube-Prometheus stack, getting this error: 
```
Unable to create application: application spec for kube-prometheus-stack is invalid: InvalidSpecError: Unable to generate manifests in prometheus: rpc error: code = Unknown desc = `helm template . --name-template kube-prometheus-stack --namespace monitor --kube-version 1.29 <api versions removed> --include-crds` failed exit status 1: Error: template: kube-prometheus-stack/templates/prometheus/prometheus.yaml:83:70: executing "kube-prometheus-stack/templates/prometheus/prometheus.yaml" at <0>: wrong type for value; expected string; got uint8 Use --debug flag to render out invalid YAML
```
This is fixed.
6. Prometheus CRD file size is larger than ArgoCD allow so sync get failed if we do Sync all Prometheus resources all at once. As a workaround, I have to prune resources in groups. 
- Workaround is not to sync CRDs/APIExtensions everytime. 
7. I had some issues with ingress. I think I fixed this by adding ClassName in application ingress configuration to match with ClassName of Nginx IngressController. 
8. Argo app of apps structure is working. Argo try to pull the helm chart and create app but when I open that app, it shows **permission denied**. I read several Github issues for it. It could be RBAC or user permissions. 
As a workaround, for now I am testing all applications by creating separate application in Argo. Once I get the fix to sync apps from app of apps, I will move these separate application helm charts to app of apps too. 
- Fixed ArgoCD configmap

# Current Issues
9. Mimir Error when deploying helm chart: 
``` 
Failed to load target state: failed to generate manifest for source 1 of 1: rpc error: code = Unknown desc = repository contains out-of-bounds symlinks. file: chart/mixins/alerts.yaml
```
After more investigation, I found Argo is blocking mimir to create symlinks. There is an old issue with similar problem: https://github.com/argoproj/argo-cd/issues/1719 but expected solution was to patch in next release. I am already using now, latest version so this bug might have came up again. Need more investigation to fix it. 

10. Distributed Loki helm chart also required promtail so deployed it also. I was able to successfully connect Loki to Grafana but still need testing to validate

11. Tempo is deployed but have to test it more

