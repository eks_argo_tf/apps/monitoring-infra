nameOverride: nginx-external
controller:
  ingressClassResource:
    name: "ingress-external"
    enabled: true
    default: false
    controllerValue: "k8s.io/ingress-external"
  updateStrategy:
    type: RollingUpdate
  # nodeSelector:
  #     app: ingress-internal
  # tolerations:
  #     - key: "app"
  #       value: "ingress-internal"
  #       effect: "NoSchedule"
  allowSnippetAnnotations: "true"
  config:
    use-forwarded-headers: "true"
    use-proxy-protocol: "false"
    force-ssl-redirect: "false"
    proxy-read-timeout: "3600"
    proxy-real-ip-cidr: 0.0.0.0/0
    proxy-send-timeout: "3600"
    real-ip-header: proxy_protocol
    set-real-ip-from: 0.0.0.0/0
  publishService:
    enabled: true
  service:
    type: LoadBalancer
    externalTrafficPolicy: Local
    targetPorts:
      http: http
      https: https
    annotations:
      service.beta.kubernetes.io/aws-load-balancer-type: nlb
      service.beta.kubernetes.io/aws-load-balancer-scheme: internet-facing
      service.beta.kubernetes.io/aws-load-balancer-external: "true"
      service.beta.kubernetes.io/aws-load-balancer-internal: "false"
      service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled: "true"

  metrics:
    enabled: true
    service:
      annotations:
        prometheus.io/scrape: "true"
        prometheus.io/port: "10254"
    serviceMonitor:
      enabled: true
      additionalLabels:
        prometheus: "default"
    prometheusRule:
      enabled: true
      additionalLabels:
        prometheus: "default"
      rules:
          - alert: NGINXConfigFailed
            expr: count(nginx_ingress_controller_config_last_reload_successful == 0) > 0
            for: 1s
            labels:
              scope: public
              component: wkb
              severity: minor
              object_name: "{{ $externalLabels.cluster_location }}-{{ $externalLabels.rc_layer }}-wkb"
            annotations:
              dn: "nginx-ingress-controller-config-invalid-{{ $externalLabels.environment }}"
              message: "Last ingress controller config reload failed"
          - alert: NGINXCertificateExpiry
            expr: (avg(nginx_ingress_controller_ssl_expire_time_seconds) by (host) - time()) < 604800
            for: 1s
            labels:
              scope: public
              component: wkb
              severity: warning
              object_name: "{{ $externalLabels.cluster_location }}-{{ $externalLabels.rc_layer }}-wkb"
            annotations:
              dn: "nginx-ingress-certificate-expiry-{{ $externalLabels.environment }}-{{ $labels.host }}"
              message: "SSL certificate for {{ $labels.host }} will expire in less than a week"
          - alert: NGINXTooMany500sByIngress
            expr: 100 * sum(rate(nginx_ingress_controller_requests{status=~"5.."}[2m])) by (ingress,controller_class) / sum(rate(nginx_ingress_controller_requests[2m])) by (ingress,controller_class) > 10
            for: 5m
            labels:
              scope: public
              component: wkb
              severity: warning
              object_name: "{{ $externalLabels.cluster_location }}-{{ $externalLabels.rc_layer }}-wkb"
            annotations:
              dn: "nginx-ingress-too-many-5xx-requests-{{ $labels.ingress}}-{{ $externalLabels.environment }}"
              message: Too many server errors. {{ printf "%0.2f" $value }}% of all requests to {{ $labels.ingress }} failed with 5xx errors.
          - alert: NGINXTooMany400sByIngress
            expr: 100 * sum(rate(nginx_ingress_controller_requests{status=~"4.."}[2m])) by (ingress,controller_class) / sum(rate(nginx_ingress_controller_requests[2m])) by (ingress,controller_class) > 10
            for: 15m
            labels:
              scope: public
              component: wkb
              severity: info
              object_name: "{{ $externalLabels.cluster_location }}-{{ $externalLabels.rc_layer }}-wkb"
            annotations:
              dn: "nginx-ingress-too-many-4xx-requests-{{ $labels.ingress}}-{{ $externalLabels.environment }}"
              message: Too many server errors. {{ printf "%0.2f" $value }}% of all requests to {{ $labels.ingress }} failed with 4xx errors.
          - alert: NGINXSlowErrorsTailTooLong
            expr: histogram_quantile(0.99, sum by (ingress,le,method,exported_namespace,exported_service,host,controller_class) (rate(nginx_ingress_controller_request_duration_seconds_bucket{status=~"[4-5].."}[2m]))) > 3
            for: 5m
            labels:
              scope: public
              component: wkb
              severity: warning
              object_name: "{{ $externalLabels.cluster_location }}-{{ $externalLabels.rc_layer }}-wkb"
            annotations:
              dn: "nginx-ingress-slow-errors-long-tail-{{ $labels.ingress}}-{{ $labels.exported_service }}-{{ $labels.method }}-{{ $labels.host}}-{{ $labels.path }}-{{ $externalLabels.environment }}"
              message:  Errors from {{ $labels.exported_service }} for method {{ $labels.method }} to {{ $labels.host }}/{{ $labels.path }} are slow to complete

  replicaCount: 1
  resources:
    limits:
      cpu: 1
      memory: 128Mi
    requests:
      cpu: 0.5
      memory: 256Mi
  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 1
